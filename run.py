import sys
import os


os.environ['DJANGO_SETTINGS_MODULE'] = 'developers_database.settings'

module_name = sys.argv[1]
function_name = ' '.join(sys.argv[2:])

exec('from %s import %s' % (module_name, function_name))
exec('%s()' % function_name)

# examples

