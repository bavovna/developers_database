import os
import sys
import argparse
import json
import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "developers_database.settings")
django.setup()

from developers.models import AssociatedEmail, Language, Developer, Project

def get_devs_based_on_params(params, type):
    params = [p.strip() for p in params.split(',')]
    search_field = '%s__icontains' % type
    result = []
    for param in params:
        if param:
            d = Developer.objects.filter(**{search_field: param})
            result.extend(d)
    return result

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Retrive existing django developers database based on"
        " required parameters")
    parser.add_argument("-f_n", "--first_names",
        help="Required first names separated by coma")
    parser.add_argument("-l_n", "--last_names",
        help="Required last names separated by coma")
    parser.add_argument("-loc", "--locations",
        help="Required locations separated by coma")
    parser.add_argument("-lang", "--languages",
        help="Required languages separated by coma")
    args = parser.parse_args()
    all_devs = []
    if args.first_names:
        all_devs.extend(
            get_devs_based_on_params(args.first_names, 'first_name'))
    if args.last_names:
        all_devs.extend(
            get_devs_based_on_params(args.last_names, 'last_name'))
    if args.locations:
        all_devs.extend(
            get_devs_based_on_params(args.locations, 'location'))
    if args.languages:
        all_devs.extend(
            get_devs_based_on_params(args.languages, 'languages__name'))
    required_devs = set(all_devs)
    emails = [dev.email for dev in required_devs if dev.email]
    skype_ids = [dev.skype_id for dev in required_devs if dev.skype_id]
    mobile_numbers = [dev.mobile_number for dev in required_devs if dev.mobile_number]
    data = {'emails': emails,
            'skype_ids': skype_ids,
            'mobile_numbers': mobile_numbers}
    data = json.dumps(data)
    print data
