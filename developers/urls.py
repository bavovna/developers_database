from django.conf.urls import patterns, url
from developers import views

urlpatterns = patterns('',
    # ex accounts/login/
    url(r'^get_info/$', views.get_info, name='get_info'),
)