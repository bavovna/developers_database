import json

from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse

from developers.models import AssociatedEmail, Language, Developer, Project


def get_info(request):
    if request.method == 'POST':
        first_names = request.POST.get('first_names', '').split(',')
        last_names = request.POST.get('last_names', '').split(',')
        locations = request.POST.get('locations', '').split(',')
        languages = request.POST.get('languages', '').split(',')

        first_names = [name.strip() for name in first_names]
        last_names = [name.strip() for name in last_names]
        locations = [name.strip() for name in locations]
        languages = [name.strip() for name in languages]

        required_devs = []
        for name in first_names:
            if name:
                d = Developer.objects.filter(first_name__icontains=name)
                required_devs.extend(d)
        for name in last_names:
            if name:
                d = Developer.objects.filter(last_name__icontains=name)
                required_devs.extend(d)
        for loc in locations:
            if loc:
                d = Developer.objects.filter(location__icontains=loc)
                required_devs.extend(d)
        for lang in languages:
            if lang:
                d = Developer.objects.filter(languages__name__icontains=lang)
                required_devs.extend(d)
        required_devs = set(required_devs)
        emails = [dev.email for dev in required_devs if dev.email]
        skype_ids = [dev.skype_id for dev in required_devs if dev.skype_id]
        mobile_numbers = [dev.mobile_number for dev in required_devs if dev.mobile_number]
        data = {'emails': emails,
                'skype_ids': skype_ids,
                'mobile_numbers': mobile_numbers}
        data = json.dumps(data)
        return HttpResponse(data)
    else:
        return render(request, 'developers/get_info.html')
