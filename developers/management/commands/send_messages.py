# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from developers.models import Project, Developer
import smtplib
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
import Skype4Py


SKYPE = Skype4Py.Skype()
SKYPE.Attach()

class Command(BaseCommand):
    def handle(self):
        sendMessages()

    def sendEmail(to, subject, text):
        ''' list, str, str -> None
        sends simple email message
        subject: string (email subject)
        text: string (email body text)
        to: a list of strings
        http://docs.python.org/2/library/smtplib.html
        '''
        email_user = 'alexwebto@hotmail.com'
        email_psw = 'calimbadeluna66' 
        
        msg = MIMEText(text)
        msg['Subject'] = subject 
        msg['From'] = 'Mark <alexwebto@hotmail.com>'#'Ukroppen Alerts'
        msg['To'] = ', '.join(to)

        s = smtplib.SMTP("smtp.live.com", 587)
    #    s.set_debuglevel(1)
        s.ehlo()
        s.starttls()

        try:
            s.login(email_user, email_psw)
        except smtplib.SMTPAuthenticationError:
            #print 'SMTPAuthenticationError'
            return False
        s.sendmail(email_user, to, msg.as_string())
        s.quit()
        
    def sendSkypeMsg(skypeHandle, message):
        ''' str -> bool
        '''
    #    skype = Skype4Py.Skype()
    #    try:
    #        skype.Attach()
    #    except KeyError:
    #        pass
        SKYPE.SendMessage(skypeHandle, message)
        return True
        
    def sendMessages():
        ''' None -> None
        sends Messages to all Developers who are involved in active Projects    
        '''
        activeProjects = Project.objects.filter(active=True)
        #print 'number of active projects: %d\n###\n\n' % activeProjects.count()
        for proj in activeProjects:
            #print 'project: %s' % proj.name
            for dev in proj.developers.all():
                #print 'developer: %s %s | skype: %s' % (dev.first_name, dev.last_name, dev.skype_id)
                # if this developer has skype, then send a skype message
                if not dev.skype_id == '':
                    #print 'sending skype message'
                    message = 'please update me on the status of this project: "%s"' % proj.name
                    sendSkypeMsg(dev.skype_id, message)
                # if the developer does not have skype, then send an email message
                elif not dev.email == '':
                    #print 'sending email'
                    subject = 'test message'
                    message = 'Hello %s,\n\nPlease update me on the status of this project: "%s"\n\nMark' % (dev.first_name, proj.name)
                    sendEmail([dev.email], subject, message)
    #            else:
                    
                    #print '%s %s: no skype, no email' % (dev.first_name, dev.last_name)
            #print '###\n\n'
