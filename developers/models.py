from django.db import models
from django.db.models import Manager
from django.db.models.query import QuerySet

# from developers.views import get_all_mails

class AssociatedEmail(models.Model):
    # all_emails = get_all_mails()
    # choices = zip(all_emails, all_emails)
    email = models.EmailField()

    def __unicode__(self):
        return self.email

class LanguageManager(Manager):
    def get_queryset(self):
        return super(LanguageManager, self).get_queryset().extra(
            select={'case_insensitive_name': 'lower(name)'}
        ).order_by('case_insensitive_name')

class Language(models.Model):
    name = models.CharField(max_length=200)

    objects = LanguageManager()

    def __unicode__(self):
        return self.name

class Developer(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200, blank=True)
    location = models.CharField(max_length=200, blank=True)
    skype_id = models.CharField(max_length=200, blank=True)
    email = models.EmailField(blank=True)
    mobile_number = models.IntegerField(blank=True, null=True)
# we do not need this field because all developers who work on the same project are in Project.developers
#    associated_developer = models.ForeignKey('self', blank=True, null=True)
    associated_email = models.EmailField(blank=True,
        help_text="This field can be filled by itself depending on the "
        "field associated_developer or populated manualy")
    languages = models.ManyToManyField(Language, blank=True)

#    def save(self, *args, **kwargs):
#        if not self.associated_email:
#            if self.associated_developer and self.associated_developer.email:
#                self.associated_email = self.associated_developer.email
#        super(Developer, self).save(*args, **kwargs)

    def __unicode__(self):
        return '{0} {1}'.format(self.first_name, self.last_name)


class Project(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField(blank=True)
    developers = models.ManyToManyField(Developer, blank=True)
    associated_emails = models.ManyToManyField(AssociatedEmail, blank=True)
    active = models.BooleanField(default=True)

    def __unicode__(self):
        return self.name

