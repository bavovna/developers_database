from django.contrib import admin

from developers.models import AssociatedEmail, Language, Developer, Project

#class ProjectInline(admin.StackedInline):
#    model = Project
#    list_display = ('name', 'active')
#    extra = 3
class ProjectAdmin(admin.ModelAdmin):
    list_display = ('name', 'active')

class DeveloperAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'skype_id', 'email')
    search_fields = ('first_name', 'last_name', 'skype_id', 'email')
    filter_horizontal = ('languages', )

admin.site.register(AssociatedEmail)
admin.site.register(Language)
admin.site.register(Developer, DeveloperAdmin)
#admin.site.register(Project)
admin.site.register(Project, ProjectAdmin)
