# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('developers', '0006_project_active'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='developer',
            name='associated_developer',
        ),
    ]
