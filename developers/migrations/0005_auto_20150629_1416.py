# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('developers', '0004_auto_20150629_1412'),
    ]

    operations = [
        migrations.AlterField(
            model_name='developer',
            name='associated_email',
            field=models.EmailField(help_text=b'This field can be filled by itself depending on the field associated_developer or populated manualy', max_length=254, blank=True),
        ),
    ]
