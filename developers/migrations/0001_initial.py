# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AssociatedEmail',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('email', models.EmailField(max_length=254)),
            ],
        ),
        migrations.CreateModel(
            name='Developer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(max_length=200)),
                ('last_name', models.CharField(max_length=200, blank=True)),
                ('location', models.CharField(max_length=200, blank=True)),
                ('skype_id', models.CharField(max_length=200, blank=True)),
                ('email', models.EmailField(max_length=254, blank=True)),
                ('mobile_number', models.IntegerField(null=True, blank=True)),
                ('associated_emails', models.ManyToManyField(to='developers.AssociatedEmail', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Language',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('description', models.TextField(blank=True)),
                ('associated_emails', models.ManyToManyField(to='developers.AssociatedEmail', blank=True)),
                ('developers', models.ManyToManyField(to='developers.Developer', blank=True)),
            ],
        ),
        migrations.AddField(
            model_name='developer',
            name='languages',
            field=models.ManyToManyField(to='developers.Language', blank=True),
        ),
    ]
