# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('developers', '0003_auto_20150629_1245'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='developer',
            name='associated_emails',
        ),
        migrations.AddField(
            model_name='developer',
            name='associated_developer',
            field=models.ForeignKey(blank=True, to='developers.Developer', null=True),
        ),
        migrations.AddField(
            model_name='developer',
            name='associated_email',
            field=models.EmailField(help_text=b'This field can be filled by itself depending on the field associated_developer or be enter manualy', max_length=254, blank=True),
        ),
    ]
